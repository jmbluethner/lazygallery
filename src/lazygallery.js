/*
 * lazygallery - A little JS & CSS snippet for a lazy loading gallery.
 *
 * @author Jan-Morris Blüthner <bluethner@heliophobix.com>
 * @copyright Do whatever you want with this. If you become a millionaire using this, you owe me a beer.
 *
 * https://heliophobix.com/
 * https://gitlab.com/nighttimedev/lazygallery
 *
 */

/*
 * @class
 * @desc Create a new Gallery instance
 * @param images > @type array (of images)
 * @param parentNodeId > @type string (Parent Node ID of the element that has to be used)
 * @param doAutoload > @type bool (perform automatic lazy-loading on while scrolling OR show a button to manually load more)
 */
class Lazygallery {
    constructor(images,parentNodeId,doAutoload) {
        this.images = images;
        this.parentNode = document.getElementById(parentNodeId);
        this.doAutoload = doAutoload;
    }
    render() {
        /* Create the container node inside the given parent node */
        let node = document.createElement('div');
        node.classList.add('lazycontainer');
        node.setAttribute('id', 'lazycontainer');
        this.parentNode.appendChild(node);
        /* Create image nodes */
        let lazycontainer = document.getElementById('lazycontainer');
        this.images.forEach(function (item, index) {
            let imgNode = document.createElement('img');
            imgNode.setAttribute('src', item);
            imgNode.classList.add('lazyimg');
            lazycontainer.appendChild(imgNode);
        });
    }
    listImages() {
        return this.images;
    }
}